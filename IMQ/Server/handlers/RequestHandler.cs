using System;
using Newtonsoft.Json;
using IMQLIbrary;

namespace ServerProgram
{
    public class RequestHandler
    {
        public SubscriberController subscriberController;
        public PublisherController publisherController;
        public ClientController clientController;

        public RequestHandler()
        {
            subscriberController = new SubscriberController();
            publisherController = new PublisherController();
            clientController = new ClientController();
        }

        public dynamic GetResponse(dynamic clientRequest)
        {
            switch (clientRequest.RequestType.ToString())
            {
                case ApplicationConstants.GetMessage:
                    string getMessageJsonRequest = RequestSerializer.SerializeClientRequest(clientRequest);
                    GetMessageRequest getMessageRequest = JsonConvert.DeserializeObject<GetMessageRequest>(getMessageJsonRequest);
                    return subscriberController.GetMessage(getMessageRequest);

                case ApplicationConstants.PushMessage:
                    string PushMessageJsonRequest = RequestSerializer.SerializeClientRequest(clientRequest);
                    PostMessageRequest PushMessageRequest = JsonConvert.DeserializeObject<PostMessageRequest>(PushMessageJsonRequest);
                    return publisherController.PushMessage(PushMessageRequest);

                case ApplicationConstants.GetTopic:
                    return clientController.GetAllTopics();

                case ApplicationConstants.GetSubscribedTopics:
                    string getAllSubscribedTopicsJsonRequest = RequestSerializer.SerializeClientRequest(clientRequest);
                    GetTopicRequest getAllSubscribedTopicsRequest = JsonConvert.DeserializeObject<GetTopicRequest>(getAllSubscribedTopicsJsonRequest);
                    return clientController.GetAllSubscribedTopics(getAllSubscribedTopicsRequest);

                case ApplicationConstants.SubscribeToTopic:
                    try
                    {
                        if (clientController.ValidateTopic(clientRequest.TopicName.ToString()))
                        {
                            string SubscribejsonRequest = JsonConvert.SerializeObject(clientRequest);
                            GetMessageRequest SubscribeRequest = JsonConvert.DeserializeObject<GetMessageRequest>(SubscribejsonRequest);

                            clientController.SubscribeToTopic(SubscribeRequest);
                        }
                        return MessageConstants.SuccessSubscription;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        return MessageConstants.TopicDoesNotExist;
                    }
                default:
                    return true;
            }
        }
    }
}