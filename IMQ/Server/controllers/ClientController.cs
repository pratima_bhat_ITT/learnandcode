using System.Collections.Generic; 
using System;
using IMQLIbrary;

namespace ServerProgram
{
    public class ClientController
    {
        private static ServerRepository _serverRepository;
        
        public ClientController()
        {
            _serverRepository = new ServerRepository();
        }
        
        public List<TopicTable> GetAllTopics()
        {
            try
            {
                return _serverRepository.GetAllTopics();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return new List<TopicTable>();
            }
        }

        public string[] GetAllSubscribedTopics(GetTopicRequest request)
        {
           try
            {
                return _serverRepository.GetAllSubscribedTopics(request);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return new string[1]{MessageConstants.NotSubscribedMessage};
            } 
        }

        public Boolean ValidateTopic(string topic) 
        {
            return _serverRepository.ValidateTopic(topic);
        }

        public void SubscribeToTopic(GetMessageRequest request)
        {
            _serverRepository.SubscribeToTopic(request);
        }

        public List<MessageTable> GetAllMessages()
        {
            return _serverRepository.GetAllMessages();
        }

        public List<MessageTable> GetAllDeadMessages()
        {
            return _serverRepository.GetAllDeadMessages();
        }
    }
}