using System;
using IMQLIbrary;

namespace ServerProgram
{
    public class PublisherController
    {
        private static PublisherRepository _publisherRepository;

        public PublisherController()
        {
            _publisherRepository = new PublisherRepository();
        }

        public string PushMessage(PostMessageRequest messageModel)
        {
            try
            {
                return _publisherRepository.PushMessage(messageModel);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return MessageConstants.ErrorInPublish;
            }
        }
    }
}