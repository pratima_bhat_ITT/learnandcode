using System.Collections.Generic; 
using System;
using IMQLIbrary ;

namespace ServerProgram
{

    public class SubscriberController
    {
        private static SubscriberRepository _subscriberRepository;
        
        public SubscriberController()
        {
            _subscriberRepository = new SubscriberRepository();
        }
        public List<MessageTable> GetMessage(GetMessageRequest messageModel)
        {
            try
            {
                return _subscriberRepository.GetMessage(messageModel);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return new List<MessageTable>();
            }
        }
    }
}