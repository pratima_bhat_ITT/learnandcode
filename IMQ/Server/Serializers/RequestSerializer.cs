using  IMQLIbrary;
using Newtonsoft.Json;
using System;

namespace ServerProgram
{
    public static class RequestSerializer
    {
        public static string SerializeClientRequest(dynamic request){
            return JsonConvert.SerializeObject(request);
        }
    }
}