using System.Linq;
using System.Collections.Generic; 
using System;
using Microsoft.EntityFrameworkCore;
using IMQLIbrary;

namespace ServerProgram
{

    public class ServerRepository
    {
        private static ClientContext _dbContext =  new ClientContext();

        public List<TopicTable> GetAllTopics()
        {
            var topics = _dbContext.topicTable.Select(row => row).ToList();
            Console.WriteLine(MessageConstants.InServerRepo+topics.Count);
            return topics;
        }

        public String[] GetAllSubscribedTopics(GetTopicRequest request)
        {
            var topics = _dbContext.subscribedTable
                        .Cast<SubscribedTopicTable>()
                        .Where(x => x.ClientName == request.clientName).ToList();
            String[] response = new String[topics.Count];
            int i=0;
            foreach(SubscribedTopicTable topic in topics)
            {
                response[i] = topic.TopicName.ToString();
                i++;
            }
            return response;
        }

        public Boolean ValidateTopic(string topic)
        {
            var topicEntry = _dbContext.topicTable.FirstOrDefault(s => s.TopicName.Contains(topic));
            if(topicEntry!= null)
            {
                return true;
            }
            return false;
        }

        public void SubscribeToTopic(GetMessageRequest request)
        {
            using (var context = new ClientContext()) 
            {
                var topicClientTable = context.subscribedTable.FirstOrDefault(s => s.ClientName.Contains(request.clientName) && s.TopicName.Contains(request.TopicName));
                if(topicClientTable == null)
                {
                    var subscribeTopic = new SubscribedTopicTable {
                        ClientName  = request.clientName,
                        TopicName = request.TopicName,
                    };
                
                    context.subscribedTable.Add(subscribeTopic);
                    context.SaveChanges();
                }
            }
        }

        public List<MessageTable> GetAllMessages()
        {
            var messages = _dbContext.messageTable.Include(MessageTable=>MessageTable.client)
            .Include(MessageTable=>MessageTable.topic)
            .Select(row => row).Where(messageTime => messageTime.expirationTime >= DateTime.Now).ToList();
            Console.WriteLine(MessageConstants.ServerRepository + messages.Count);
            return messages;
        }

        public List<MessageTable> GetAllDeadMessages()
        {
            var messages = _dbContext.messageTable
            .Include(MessageTable=>MessageTable.client)
            .Include(MessageTable=>MessageTable.topic)
            .Select(row => row).Where(messageTime => messageTime.expirationTime < DateTime.Now).ToList();
            Console.WriteLine(MessageConstants.ServerRepository + messages.Count);
            return messages;
        }

    }
}