using System.Linq;
using System.Collections.Generic; 
using Microsoft.EntityFrameworkCore;
using System;
using IMQLIbrary;

namespace ServerProgram
{

    public class SubscriberRepository
    {
        private static ClientContext _dbContext =  new ClientContext();
        
        public List<MessageTable> GetMessage(GetMessageRequest messageModel)
        {
            var topic = _dbContext.topicTable.FirstOrDefault(s => s.TopicName.Contains(messageModel.TopicName));
            var subscribedTopic = _dbContext.subscribedTable.FirstOrDefault(s => s.ClientName.Contains(messageModel.clientName) && s.TopicName.Contains(messageModel.TopicName));

            if(subscribedTopic != null)
            {
                var messages = _dbContext.messageTable
                              .Include(MessageTable=>MessageTable.client)
                              .Include(MessageTable=>MessageTable.topic)
                              .Cast<MessageTable>()
                              .Where(x => x.topic.Id == topic.Id && x.expirationTime >= DateTime.Now).ToList();
                return messages;
            }
            return new List<MessageTable>();
        }
    }
}