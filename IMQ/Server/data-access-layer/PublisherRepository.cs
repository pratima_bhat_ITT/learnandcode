using System.Linq;
using System;
using IMQLIbrary;

namespace ServerProgram
{

    public class PublisherRepository
    {
        public PublisherRepository(){
            _savePublisherDetails = new ClientContext ();
        }

        public PublisherRepository(ClientContext cntx){
            _savePublisherDetails = cntx;
        }

        private readonly ClientContext _savePublisherDetails =  null;
        QueueService queueService = QueueInstance.InstanceCreation() ;
    
        public string PushMessage(PostMessageRequest messageModel)
        {
            using (var context = new ClientContext()) 
            {   
                var rootTableEntry = context.rootTable.FirstOrDefault(s => s.ClientName.Contains(messageModel.clientName));
                var messageTopic = context.topicTable.FirstOrDefault(m => m.TopicName.Contains(messageModel.TopicName));
                var subscribedTopic = context.subscribedTable.FirstOrDefault(s => s.ClientName.Contains(messageModel.clientName) && s.TopicName.Contains(messageModel.TopicName));

                if(rootTableEntry != null && subscribedTopic != null)
                {
                    var message = new MessageTable {
                    client = rootTableEntry,
                    topic = messageTopic,
                    message = messageModel.Message,
                    createdDate = messageModel.CreatedDate,
                    expirationTime = messageModel.ExpirationTime
                    };
                    context.messageTable.Add(message);
                    context.SaveChanges();
                    queueService.AddMessage(message);
                    queueService.DisplayAllMessagesFromQueue();
                    return messageModel.Message;
                }
                if(messageTopic == null)
                {
                    return MessageConstants.TopicDoesNotExist;
                }
                else
                {
                    return MessageConstants.TopicDoesNotExist;
                }
            }
        }

        public dynamic createMessageTableInstance  (dynamic rootTableEntry,  dynamic subscribedTopic, PostMessageRequest messageModel){
            var message = new MessageTable {
                    client = rootTableEntry,
                    topic = subscribedTopic,
                    message = messageModel.Message,
                    createdDate = messageModel.CreatedDate,
                    expirationTime = messageModel.ExpirationTime
                    };
            return message;
        }
    }
}