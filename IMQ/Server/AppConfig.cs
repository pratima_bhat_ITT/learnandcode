namespace ServerProgram
{
    public static class AppConfig
    {
        public const string IpAddress = "127.0.0.1";
        public const int PortNumber = 5000;
        public const string FileLocation  = "./Files/";
        public const string DbConnectionString  = "Server=.\\SQLEXPRESS; Database=IMQ;Trusted_Connection=true";
        public const string RootTableCreateQuery  = "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Root')" + 
            "BEGIN CREATE TABLE Root (Id INT IDENTITY(1,1) PRIMARY KEY, ClientName VARCHAR(40) ) END";
        public const string ClientTableCreateQuery  = "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Client') BEGIN CREATE TABLE " + 
            "Client (Id INT IDENTITY(1,1) PRIMARY KEY, Data VARCHAR(200), TimeStamp DATETIME, RootId Int," + 
            "CONSTRAINT [FK_Client_Root] FOREIGN KEY ([RootId]) REFERENCES [Root] ([Id]) ) END";
  
    }
}