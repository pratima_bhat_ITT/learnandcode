﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class AddTopics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "rootTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientName = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rootTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "subscribedTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientName = table.Column<string>(nullable: true),
                    TopicName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subscribedTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "topicTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TopicName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_topicTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "messageTable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    clientId = table.Column<int>(nullable: false),
                    topicId = table.Column<int>(nullable: false),
                    message = table.Column<string>(nullable: true),
                    createdDate = table.Column<DateTime>(nullable: false),
                    expirationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_messageTable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_messageTable_rootTable_clientId",
                        column: x => x.clientId,
                        principalTable: "rootTable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_messageTable_topicTable_topicId",
                        column: x => x.topicId,
                        principalTable: "topicTable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "topicTable",
                columns: new[] { "Id", "TopicName" },
                values: new object[] { 1, "Learn&Code" });

            migrationBuilder.InsertData(
                table: "topicTable",
                columns: new[] { "Id", "TopicName" },
                values: new object[] { 2, "Learn&Grow" });

            migrationBuilder.CreateIndex(
                name: "IX_messageTable_clientId",
                table: "messageTable",
                column: "clientId");

            migrationBuilder.CreateIndex(
                name: "IX_messageTable_topicId",
                table: "messageTable",
                column: "topicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "messageTable");

            migrationBuilder.DropTable(
                name: "subscribedTable");

            migrationBuilder.DropTable(
                name: "rootTable");

            migrationBuilder.DropTable(
                name: "topicTable");
        }
    }
}
