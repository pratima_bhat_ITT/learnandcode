﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class Addqueue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_messageTable_rootTable_clientId",
                table: "messageTable");

            migrationBuilder.DropForeignKey(
                name: "FK_messageTable_topicTable_topicId",
                table: "messageTable");

            migrationBuilder.AlterColumn<int>(
                name: "topicId",
                table: "messageTable",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "clientId",
                table: "messageTable",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_messageTable_rootTable_clientId",
                table: "messageTable",
                column: "clientId",
                principalTable: "rootTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_messageTable_topicTable_topicId",
                table: "messageTable",
                column: "topicId",
                principalTable: "topicTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_messageTable_rootTable_clientId",
                table: "messageTable");

            migrationBuilder.DropForeignKey(
                name: "FK_messageTable_topicTable_topicId",
                table: "messageTable");

            migrationBuilder.AlterColumn<int>(
                name: "topicId",
                table: "messageTable",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "clientId",
                table: "messageTable",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_messageTable_rootTable_clientId",
                table: "messageTable",
                column: "clientId",
                principalTable: "rootTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_messageTable_topicTable_topicId",
                table: "messageTable",
                column: "topicId",
                principalTable: "topicTable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
