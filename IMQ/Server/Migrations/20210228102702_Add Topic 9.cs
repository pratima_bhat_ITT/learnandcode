﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class AddTopic9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "topicTable",
                columns: new[] { "Id", "TopicName" },
                values: new object[,]
                {
                    { 3, "UBI" },
                    { 4, "SPORTS" },
                    { 5, "ELECTION" },
                    { 6, "TECH" },
                    { 7, "CINEMA" },
                    { 8, "SCIENCE DAY" },
                    { 9, "Busy DAY" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "topicTable",
                keyColumn: "Id",
                keyValue: 9);
        }
    }
}
