 namespace ServerProgram{
 public sealed class QueueInstance
{
    private static QueueService queueService = new QueueService();

    public static QueueService InstanceCreation()
    {
        if(queueService==null){
            lock (queueService)
            {
                if (queueService==null)
                {
                    queueService = new QueueService();
                }
            }
        }
        return queueService;
    }
}
 }