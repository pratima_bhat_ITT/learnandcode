using Microsoft.EntityFrameworkCore;
using System.Linq;
using IMQLIbrary;

namespace ServerProgram
{
    public class ClientContext : DbContext
    {
        public DbSet<MessageTable> messageTable { get; set; }
        public DbSet<RootTable> rootTable { get; set; }
        public DbSet<TopicTable> topicTable { get; set; }
        public DbSet<SubscribedTopicTable> subscribedTable { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@MessageConstants.DatabaseConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
             modelBuilder.Entity<TopicTable>().HasData(TopicConstants.topics());
        }
    }
}
