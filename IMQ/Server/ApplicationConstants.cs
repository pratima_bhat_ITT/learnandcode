namespace ServerProgram
{
    public static class ApplicationConstants
    {

        public const string GetMessage = "GetMessageRequest";
        public const string PushMessage = "PostMessageRequest";
        public const string GetTopic = "GetTopic";
        public const string SubscribeToTopic = "SubscribeToTopic";
        public const string GetSubscribedTopics = "GetSubscribedTopicRequest";
    }
}