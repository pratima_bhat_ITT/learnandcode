using IMQLIbrary;
namespace  ServerProgram
{
    public static class TopicConstants
    {
        public static TopicTable[] topics()
        {
            var topic = new TopicTable[] {
                    new TopicTable{Id=1,TopicName="Learn&Code"},
                    new TopicTable{Id=2,TopicName="Learn&Grow"},
                    new TopicTable{Id=3,TopicName="UBI"},
                    new TopicTable{Id=4,TopicName="SPORTS"},
                    new TopicTable{Id=5,TopicName="ELECTION"},
                    new TopicTable{Id=6,TopicName="TECH"},
                    new TopicTable{Id=7,TopicName="CINEMA"},
                    new TopicTable{Id=8,TopicName="SCIENCE DAY"},
                    new TopicTable{Id=9,TopicName="Busy DAY"},
            };
            return topic;
        }
    }
}