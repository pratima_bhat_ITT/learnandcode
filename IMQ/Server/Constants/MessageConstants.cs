namespace ServerProgram
{
    public static class MessageConstants 
    {
        public const string NotSubscribedMessage = "Sorry You have not subscribed";
        public const string TopicDoesNotExist = "Sorry, the topic doesn't exist";
        public const string ServerRepository = "server Repository";
        public const string InServerRepo = "IN server repo";
        public const string SuccessMessage = "Subscribed successfully";
        public const string GetMessage = "GetMessageRequest";
        public const string PushMessage = "PostMessageRequest";
        public const string GetTopic = "GetTopic";
        public const string SubscribeToTopic = "SubscribeToTopic";

        public const string Publisher = "publisher";
        public const string Subscriber = "subscriber";

        public const string ObjectDisposedException = "Object Disposed Exception : ";
        public const string ArgumentNullException = "Argument Null Exception : ";
        public const string InvalidOperationException = "Invalid Operation Exception : ";
        public const string ArgumentOutOfRangeException = "Argument Out Of Range Exception : ";
        public const string SqlException = "SQL Exception : ";
        public const string FormatException = "Format Exception: ";
        public const string SocketException = "Socket Exception : ";

        public const string ServerStarted = "Server Started";
        public const string SentMessage  = "Sent: ";
        public const string RecievedMessage = "Recieved: ";
        public const string WaitingForConnection = "Waiting for a connection...";
        public const string Connected = "Connected!";
        public const int ByteArraySize = 2048;
        public const string ServerRepo = "server Repository" ;

        public const string MessageId = "Message Id";
        public const string PublisherId = "Publisher Id  : 5";
        public const string ExpirationTime= "Expiration Time";
        public const string TopicId = "Topic Id :";
        public const string Message =  "Message \n";
        public const string CreatedTime = "Created Time:";
        public const string Key = "Key :";
        public const string QueueService = "Queue Service : ";
        public const string ErrorInPublish = "Could not push the message";
        public const string ErrorInTopic = "The topic does not exist!!";
        public const string SuccessSubscription = "Successfully subscribed!!";
        public const string GetSubscribedTopics = "GetSubscribedTopicRequest";
        public const string DatabaseConnectionString = "Server=ITT-PRATIMA;Database=IMQDatabase;Trusted_Connection=True;MultipleActiveResultSets=true"; 
        public const string DeadMessage ="DEAD MESSAGES" ;
        public const string Topic = "Topic";
    }
}