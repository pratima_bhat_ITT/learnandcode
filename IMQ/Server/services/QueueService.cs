using System.Linq;
using System.Collections.Generic; 
using System;
using IMQLIbrary; 

namespace ServerProgram
{
    public class QueueService
    {
        public Dictionary<string, List<MessageTable>> messageQueue;
        public Dictionary<string, List<MessageTable>> deadMessageQueue;
        public ClientController clientController;

        public QueueService()
        {
            Console.WriteLine(MessageConstants.QueueService);
            clientController = new ClientController();
            messageQueue = new Dictionary<string, List<MessageTable>>();
            PopulateMessageQueue();
		}

        public void PopulateMessageQueue()
        {
            var messages = clientController.GetAllMessages();
            foreach(MessageTable message in messages)
            {
                AddMessage(message);
            }
            DisplayAllMessagesFromQueue();
		}

        public void AddMessage(MessageTable message)
        {
            if(messageQueue.ContainsKey(message.topic.TopicName))
            {
                var messageList = messageQueue[message.topic.TopicName] ;
                messageList.Add(message);
            }
            else
            {
                List<MessageTable> list = new List<MessageTable>();
                list.Add(message);
                messageQueue.Add(message.topic.TopicName, list);
            }
        }
         public void AddDeadMessage(MessageTable message)
        {
            if(deadMessageQueue.ContainsKey(message.topic.TopicName))
            {
                var messageList = deadMessageQueue[message.topic.TopicName] ;
                messageList.Add(message);
            }
            else
            {
                List<MessageTable> list = new List<MessageTable>();
                list.Add(message);
                deadMessageQueue.Add(message.topic.TopicName, list);
            }
        }

        public void DisplayAllDeadMessages()
        {
            Console.WriteLine(MessageConstants.DeadMessage);
            foreach(KeyValuePair<string, List<MessageTable>> kvp in deadMessageQueue)
            {
                Console.WriteLine(MessageConstants.Topic + kvp.Key);
                var messages = kvp.Value;
                foreach(MessageTable message in messages)
                {
                    Console.WriteLine("------------------------");
                    Console.WriteLine(MessageConstants.PublisherId + message.client.Id +MessageConstants.Message+ message.message +MessageConstants.ExpirationTime + message.expirationTime);
                }
            }
        }

        public void DisplayAllMessagesFromQueue()
        {
            foreach(KeyValuePair<string, List<MessageTable>> queue in messageQueue)
            {
                Console.WriteLine(MessageConstants.Key + queue.Key);
                var messages = queue.Value;
                foreach(MessageTable message in messages)
                {
                    Console.WriteLine(MessageConstants.PublisherId + message.client.Id +MessageConstants.Message+ message.message +MessageConstants.ExpirationTime + message.expirationTime);
                }
            }
        }
    }
}