using System;

namespace ServerProgram
{
    class ServerStartup
    {
        static void Main(string[] args)
        {
            try 
            {
                QueueService qs = new QueueService();
                Server serverInstance = new Server(AppConfig.IpAddress, AppConfig.PortNumber);
                Console.WriteLine(MessageConstants.ServerStarted);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}