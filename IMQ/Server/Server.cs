﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace ServerProgram
{
    public class Server
    {

        TcpListener server = null;
        public Server(string ip, int port)
        {
            try
            {
                IPAddress ipAddress = IPAddress.Parse(ip);
                server = new TcpListener(ipAddress, port);
                server.Start();
                StartListening();
            }
            catch (FormatException ex)
            {
                throw new Exception(MessageConstants.FormatException + ex.Message);
            }
        }

        public void StartListening()
        {
            try
            {
                while (true)
                {
                    Console.WriteLine(MessageConstants.WaitingForConnection);
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine(MessageConstants.Connected);
                    Thread imqThread = new Thread(new ParameterizedThreadStart(HandleClient));
                    imqThread.Start(client);
                }
            }
            catch (SocketException ex)
            {
                server.Stop();
                throw new Exception(MessageConstants.SocketException + ex.Message);
            }
        }

        public void HandleClient(Object obj)
        {
            TcpClient client = (TcpClient)obj;
            var stream = client.GetStream();
            string data = null;
            Byte[] bytes = new Byte[MessageConstants.ByteArraySize];
            string fileName = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString() + "_" 
                + ((IPEndPoint)client.Client.RemoteEndPoint).Port.ToString ();

            try
            {
                HandleRequest(data, bytes,stream);
            }
            
            catch(ObjectDisposedException ex)
            {
                client.Close();
                throw new Exception(MessageConstants.ObjectDisposedException + ex.Message);
            }
            catch(InvalidOperationException ex)
            {
                client.Close();
                throw new Exception(MessageConstants.InvalidOperationException + ex.Message);
            }
            catch(Exception ex)
            {
                client.Close();
                Console.WriteLine(ex);
            }
        }

        public void HandleRequest(string data,  Byte[] bytes,dynamic stream){
            data = Encoding.ASCII.GetString(bytes, 0, stream.Read(bytes, 0, bytes.Length));
            Console.WriteLine("Data is"+data);
            dynamic receivedRequest = JsonConvert.DeserializeObject(data);
            Console.WriteLine(MessageConstants.RecievedMessage + receivedRequest);           
            RequestHandler handleRequest = new RequestHandler();
            var reponse = handleRequest.GetResponse(receivedRequest);
            string jsonData = JsonConvert.SerializeObject(reponse);
            Byte[] reply = Encoding.ASCII.GetBytes(jsonData);
            Console.WriteLine(jsonData);
            stream.Write(reply, NumericConstants.zero, reply.Length);
            Console.WriteLine(MessageConstants.SentMessage + reponse);
        }
    }
}