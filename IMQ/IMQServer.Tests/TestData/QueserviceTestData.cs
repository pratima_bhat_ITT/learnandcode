using IMQLIbrary;
using System;

namespace IMQServer.Tests
{
    public static class QueueServiceTestData
    {
        public static MessageTable message = new MessageTable(){
            Id = 1,
            clientId = 1002,
            client = new RootTable{
                Id = 1002,
                ClientName = "Akshata",
                Role = "Publisher"
            },
            topicId = 1,
            topic = new TopicTable{
                Id = 2,
                TopicName = "Learn&Grow"
            },
            message = "Heyyy",
            createdDate = DateTime.Now,
            expirationTime = DateTime.Now,
        };
    }
}
