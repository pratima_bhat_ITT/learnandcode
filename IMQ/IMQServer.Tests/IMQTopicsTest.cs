using Xunit;
using ServerProgram;
using IMQLIbrary;

namespace IMQServer.Tests {
    public class IMQTopicsTest{
        [Fact] 
        public void TopicConstantValuesTest() 
        { 
            var topics = TopicConstants.topics();      
            Assert.IsType<TopicTable[]>(topics);
        } 
    }
}