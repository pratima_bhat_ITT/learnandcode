using System;
using Xunit;
using ServerProgram;
using IMQLIbrary;

namespace IMQServer.Tests
{
    public class SerializerTest
    {
      
        PostMessageRequest dataRequest = new PostMessageRequest(){
            Header ="Client-Header",
            RequestFormat = "json",
            TopicName ="CINEMA",
            Message ="Hey there",
            CreatedDate = DateTime.Now,
            ExpirationTime = DateTime.Now,
            clientName= "pratima",
            RequestType= "PostMessageRequest",
            Role= "Publisher"
        } ;  
        [Fact]
        public void ShouldSerialzeRequest()
        {
            dynamic request = RequestSerializer.SerializeClientRequest(dataRequest);
            Assert.IsType<String>(request);
        }
    }
}
