using Xunit;
using ServerProgram;

namespace IMQServer.Tests
{
    public class QueueServiceTests
    {
        QueueService queueService = QueueInstance.InstanceCreation() ;

        [Fact]
        public void shouldDisplayAllMessagesFromQueue()
        {
            queueService.DisplayAllMessagesFromQueue();
            Assert.True(true);
        }

        [Fact]
        public void shouldAddMessage()
        {
            queueService.AddMessage(QueueServiceTestData.message);
            Assert.True(true);
        }
    
    }
}