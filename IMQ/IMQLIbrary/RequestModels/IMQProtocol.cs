using Newtonsoft.Json;
namespace IMQLIbrary
{
    public class IMQProtocol
    {
        public string Header = "Client-Header";
        public string clientName {get; set;}
        public string RequestType {get; set;}
        public string RequestFormat = "json";
        public string Role{get;set;}
    }
}
