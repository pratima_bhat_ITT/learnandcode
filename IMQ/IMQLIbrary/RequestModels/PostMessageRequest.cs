using Newtonsoft.Json;
using System;
namespace IMQLIbrary
{
    public class PostMessageRequest : IMQProtocol
    {
        public string TopicName {get; set;}
        public string Message {get; set;}
        public DateTime CreatedDate { get; set;}
        public DateTime ExpirationTime { get; set;}
        
    }
}
