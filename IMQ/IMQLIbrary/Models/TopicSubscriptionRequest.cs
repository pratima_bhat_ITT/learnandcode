using Newtonsoft.Json;
namespace IMQLIbrary
{
    public class TopicSubscriptionRequest : IMQProtocol
    {
        public string TopicName {get; set;}
    }
}