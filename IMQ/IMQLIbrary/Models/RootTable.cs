namespace IMQLIbrary{

    public class RootTable {
        public int Id  { get; set; }
        public string ClientName { get; set; }
        public string Role { get; set; }
    }
}