

namespace IMQLIbrary{
    public class SubscribedTopicTable
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string TopicName { get; set; }
    }
}
