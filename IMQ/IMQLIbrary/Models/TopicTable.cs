namespace IMQLIbrary{

    public class TopicTable {
        public int Id  { get; set; }
        public string TopicName { get; set; }
    }
}