using System;

namespace IMQLIbrary
{ 
    public class MessageTable
    {
        public int Id { get; set; }
        public RootTable client { get; set; }
        public int clientId {get; set; }
        public int topicId {get; set; }
        public TopicTable topic { get; set; }
        public string message { get; set; }
        public DateTime createdDate { get; set;}
        public DateTime expirationTime { get; set;}
    }
}
