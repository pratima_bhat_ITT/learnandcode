using Microsoft.EntityFrameworkCore;
using IMQLIbrary;

namespace ClientProgram
{
    public class ClientContext : DbContext
    {
        public DbSet<RootTable> rootTable { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@MessageConstants.DatabaseConnectionString);
        }
    }
}
