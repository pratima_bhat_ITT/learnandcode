﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ClientProgram
{
    class ClientStartup 
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine(MessageConstants.ClientProgram);
            Client _client = new Client();
            _client.ClientProcess();
        }

    }
}