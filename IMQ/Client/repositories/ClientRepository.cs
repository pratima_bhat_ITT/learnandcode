using System;
using System.Linq;
using IMQLIbrary;

namespace ClientProgram{

    public class ClientRepository {
        private ClientContext db_context;
        public ClientRepository(){
            db_context = new ClientContext();
        }
        public ClientRepository(ClientContext context){
           db_context = context;
        }
        public void InsertIntoRootTable(RootTable client){
            using (var context = db_context) 
            {
                var rootTableEntry = context.rootTable.FirstOrDefault(s => s.ClientName.Contains(client.ClientName) && s.Role.Contains(client.Role));
                if( rootTableEntry == null ) 
                {
                    context.rootTable.Add(client);
                    context.SaveChanges();
                }
                else
                {
                    Console.WriteLine(MessageConstants.EntryExist);
                }
            }
        }
    }
}