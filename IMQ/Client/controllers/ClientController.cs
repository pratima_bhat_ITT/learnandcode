using IMQLIbrary;

namespace ClientProgram
{
    public class ClientController
    {
        private static ClientRepository _clientRepository;
        public ClientController()
        {
            _clientRepository = new ClientRepository();
        }
        public ClientController (ClientRepository clientcontroller){
            _clientRepository = clientcontroller;
        }
        public void InsertIntoRootTable(RootTable client) {
            _clientRepository.InsertIntoRootTable(client);
        }

        public void StoreClientDetails(string _clientName,string _clientRole)
        {
            var newClient = new RootTable()
            {
                ClientName = _clientName,
                Role = _clientRole
            };
            InsertIntoRootTable(newClient);
        }
    }
}