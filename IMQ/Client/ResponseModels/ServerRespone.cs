using System;
using System.Collections.Generic;
using IMQLIbrary;

namespace ClientProgram
{
    public class ServerResponse
    {
        public void DisplayAllTopics(List<TopicTable> topics)
        {
            Console.WriteLine(MessageConstants.availableTopics);
            Console.WriteLine("----------------");
            Console.WriteLine(MessageConstants.TopicName);
            foreach (TopicTable topic in topics)
            {
                Console.WriteLine("* " + topic.TopicName);
            }
        }

        public void DisplayAllMessages(List<MessageTable> messages)
        {
            if(messages.Count == NumericConstants.zero){
                Console.WriteLine(MessageConstants.NoMessage);
            }
            else{
                foreach (MessageTable message in messages)
                {
                    Console.WriteLine("----------------");
                    Console.WriteLine(MessageConstants.PublisherId + message.client.Id + MessageConstants.Message + message.message + MessageConstants.ExpirationTime + message.expirationTime);
                
                }
            }
        }

        public void CheckResponse(string response)
        {
            if (response.Length == NumericConstants.ResponsEmptyArrayLength)
                Console.WriteLine(MessageConstants.NotSubScribed);
            else
                Console.WriteLine(response);
        }
    }
}