using System.Security.Cryptography.X509Certificates;
namespace ClientProgram
{
    public static class MessageConstants
    {
        public const string ClientNameInput = "Enter your name";

        public const string ChooseRole = " Choose your Role 1. Publisher 2. Subscriber";

        public const string WrongChoiceWarningMesaage = "Please select either 1 or 2";

        public const string SentMessage = "Sent Message: ";

        public const string RecievedMessage = "Recieved Message: ";

        public const string PressAnyKeyToExitMessage = "Press any key to exit";

        public const string SocketException = "Socket Exception : ";

        public const string Publisher = "Publisher";

        public const string ReadSubscriberdTopicName = "Enter the topic to be Subscribed: ";

        public const string SubscribeToTopic = "SubscribeToTopic";

        public const string MessageFromServer = "Message from your Server!";

        public const string UserChoice = "1. Get all topics \n2. Subscribe to a topic \n3. Push a message \n4. Get your subscribed Topics  \n5. Exit";

        public const string EnterValidOption = "Please Select one of the mentioned option";

        public const string Subscriber = "Subscriber";

        public const string SubscribedTopicMessage = "Your subscribed topics are following :";

        public const string EnterTopicName = "Please Enter the topic name that you want to subscribe : ";

        public const string ReadTopicName = "Please Enter the topic name";

        public const string GetMessage = "GetMessageRequest";

        public const string PushMessage = "PostMessageRequest";

        public const string GetTopic = "GetTopic";

        public const string SuccessSubcribe = "Successfully subscribed!!";

        public const string ErrorInSubscription = "The topic you want to subscribe does not exist!!";

        public const string DatabaseConnectionString = "Server=ITT-PRATIMA;Database=IMQDatabase;Trusted_Connection=True;MultipleActiveResultSets=true"; 
       
        public const string GetSubscribedTopics = "GetSubscribedTopicRequest";
        
        public const string EnterChoice = "Enter your Choice";

        public const string EnterValidChoice = "Enter the valid choice";

        public const string EntryExist = "The Entry already exist!!";

        public const string TopicId ="Topics Id";

        public const string TopicName ="\n Topics Name";

        public const string availableTopics = "Available Topics on server:";

        public const string GetSubscriberChoice = "1. Get all topics \n2. Subscribe to a topic \n3. Read Messages \n4. Get your subscribed Topics \n5. Exit";
        
        public const string MessageId = "Message Id";

        public const string PublisherId = "Publisher Id : ";

        public const string ExpirationTime= "  Expiration Time";

        public const string Message =  "\n Message : ";

        public const string CreatedTime = "Created Time:";

        public const string EnterTime = "Enter expiration time for your message in minutes: ";

        public const string ClientProgram = "Client Program!!";

        public const string NotSubScribed = "Sorry You have Not subscribed to Any Topics Yet";
        public const string NoMessage = "No Message available at the moment!";
    }
}