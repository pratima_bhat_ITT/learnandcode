 namespace ClientProgram
{
    public static class NumericConstants
    {
        public const int ByteArraySize = 2048;
        public const int ResponsEmptyArrayLength = 2;
        public const int zero = 0;
    }
}