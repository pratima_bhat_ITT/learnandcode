namespace ClientProgram
{
    public static class AppConfig
    {
        public const string IpAddress = "127.0.0.1";
        public const int PortNumber = 5000;
    }
}