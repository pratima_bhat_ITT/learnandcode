using System;
using IMQLIbrary;

namespace ClientProgram
{
    public class Subscriber
    {
        private static string _subscriberName;
        private ClientController _clientController;
        private static RequestResponseService _requestResponseService;
        private static TopicService _topicService;
        private static ServerResponse _responseHelper;

        public Subscriber()
        {
            _clientController = new ClientController();
            _requestResponseService = new RequestResponseService();
            _topicService = new TopicService();
            _responseHelper = new ServerResponse();
        }
        public void SubscriberProcess( string SubscriberName){
           SetClientName(SubscriberName) ;
            _clientController.StoreClientDetails(_subscriberName,MessageConstants.Subscriber);
            PerformSubscriberOperations();
        }
        public void SetClientName(string name)
        {
            _subscriberName = name;
        }

        private void PerformSubscriberOperations()
        {
            int choice;
            dynamic response;
            do
            {
                Console.WriteLine(MessageConstants.EnterChoice);
                Console.WriteLine(MessageConstants.GetSubscriberChoice);
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        response = _topicService.GetAllTopics(_subscriberName, MessageConstants.Subscriber);
                        var topics = _requestResponseService.DeserializeGetAllTopics(response);
                        _responseHelper.DisplayAllTopics(topics);
                        break;

                    case 2:
                        response = _topicService.SubscribeToTopic(_subscriberName, MessageConstants.Subscriber);
                        Console.WriteLine(response);
                        break;

                    case 3:
                        GetMessage();
                        break;
                    case 4:
                        response = _topicService.GetAllSubscribedTopics(_subscriberName, MessageConstants.Subscriber);
                        _responseHelper.CheckResponse(response);
                        break;
                    case 5: return;

                    default:
                        Console.WriteLine(MessageConstants.EnterValidChoice);
                        break;
                }
            } while (choice != 5);
        }

        public void GetMessage()
        {
            GetMessageRequest request = new GetMessageRequest
            {
                clientName = _subscriberName,
                RequestType = MessageConstants.GetMessage,
                Role = MessageConstants.Subscriber
            };
            Console.WriteLine(MessageConstants.EnterTopicName);
            request.TopicName = _requestResponseService.GetTopicFromUser();
            string response = SocketService.ConnectToServer(request);
            var messages = _requestResponseService.DeserializeGetAllMessages(response);
            _responseHelper.DisplayAllMessages(messages);
        }
    }
}