using System;
using IMQLIbrary;

namespace ClientProgram
{
    public class Publisher
    {
        private static string _publisherName;
        private ClientController _clientController;
        private static RequestResponseService _requestResponseService;
        private static TopicService _topicService;
        private static ServerResponse _responseHelper;
        int choice = 0;

        public Publisher()
        {
            _requestResponseService = new RequestResponseService();
            _clientController = new ClientController();
            _topicService = new TopicService();
            _responseHelper = new ServerResponse();
        }

        public void PublisherProcess( string clientName){
            SetClientName(clientName);
             _clientController.StoreClientDetails(_publisherName,MessageConstants.Publisher);
            PerformPublisherOperations();
        }
        public void SetClientName(string name)
        {
            _publisherName = name;
        }

        private void PerformPublisherOperations()
        {
            string response;
            do
            {
                choice = getPublisherChoice();
                switch (choice)
                {
                    case 1:
                        response = _topicService.GetAllTopics(_publisherName, MessageConstants.Publisher);
                        var topics = _requestResponseService.DeserializeGetAllTopics(response);
                        _responseHelper.DisplayAllTopics(topics);
                        break;

                    case 2:
                        response = _topicService.SubscribeToTopic(_publisherName, MessageConstants.Publisher);
                        Console.WriteLine(response);
                        break;

                    case 3:
                        PostMessage();
                        break;
                    case 4:
                        response = _topicService.GetAllSubscribedTopics(_publisherName, MessageConstants.Publisher);
                        _responseHelper.CheckResponse(response);
                        break;

                    case 5: return;

                    default:
                        Console.WriteLine(MessageConstants.EnterValidOption);
                        break;
                }
            } while (choice != 5);
        }

        private static void PostMessage()
        {
            PostMessageRequest request = _requestResponseService.CreateRequest();
            request.clientName = _publisherName;
            request.RequestType = MessageConstants.PushMessage;
            request.Role = MessageConstants.Publisher;
            var resposne = SocketService.ConnectToServer(request);
            Console.WriteLine(resposne);
        }

        private int getPublisherChoice()
        {
            Console.WriteLine(MessageConstants.EnterChoice);
            Console.WriteLine(MessageConstants.UserChoice);
            try
            {
                choice = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception exception)
            {

            }

            return choice;
        }
    }
}