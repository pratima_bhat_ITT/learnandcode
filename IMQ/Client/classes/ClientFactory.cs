using System;

namespace ClientProgram
{
    public class ClientFactory
    {
        private ClientRoles _clientType;
        public ClientFactory(ClientRoles type)
        {
            _clientType = type;
        }

        public ClientFactory() { }
        public dynamic GetObject(ClientRoles type)
        {
            _clientType = type;
            dynamic clientObject = null;
            switch (_clientType)
            {
                case ClientRoles.Publisher:
                    clientObject = new Publisher();
                    break;
                case ClientRoles.Subscriber:
                    clientObject = new Subscriber();
                    break;
                default:
                    clientObject = null;
                    break;
            }
            return clientObject;
        }
    }
}
