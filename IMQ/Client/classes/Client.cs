using System;
using IMQLIbrary;

namespace ClientProgram
{
    public class Client
    {
        private string _clientName;
        private int _chooseRole;
        private ClientFactory clientFactory = new ClientFactory();
        private dynamic user;

        public Client(string clientName, int role)
        {
            _clientName = clientName;
            _chooseRole = role;
        }

        public Client() { }
        public void ClientProcess()
        {
            ReadClientName();
            GetRole();
            InitializeClientBasedOnRole();
        }

        public void ReadClientName()
        {
            Console.WriteLine(MessageConstants.ClientNameInput);
            _clientName = Console.ReadLine();
        }

        public void GetRole()
        {
            Console.WriteLine(MessageConstants.ChooseRole);
            _chooseRole = Convert.ToInt32(Console.ReadLine());
        }

        public void InitializeClientBasedOnRole()
        {
            switch (_chooseRole)
            {
                case 1:
                    user = clientFactory.GetObject(ClientRoles.Publisher);
                    user.PublisherProcess(_clientName);
                    break;

                case 2:
                    user = clientFactory.GetObject(ClientRoles.Subscriber);
                    user.SubscriberProcess(_clientName);
                    break;
                default:
                    Console.WriteLine(MessageConstants.WrongChoiceWarningMesaage);
                    ClientProcess();
                    break;
            }
        }
    }
}