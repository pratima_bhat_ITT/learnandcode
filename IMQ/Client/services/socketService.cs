using System;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace ClientProgram
{
    
    public class SocketService
    {
        
        public static string ConnectToServer(dynamic request) 
        {
            String response = null;
            try 
            {
                TcpClient client = new TcpClient(AppConfig.IpAddress, AppConfig.PortNumber);
                NetworkStream stream = client.GetStream();
                string jsonData = JsonConvert.SerializeObject(request);
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(jsonData);   
                stream.Write(data, 0, data.Length);
                response =  response = SendResponse(data,stream,response); ;         
                client.Close();  
                stream.Close();
            } 
            catch (SocketException ex) 
            {
                Console.WriteLine(MessageConstants.SocketException + ex.Message);
            }
            return response;
        }

        public static dynamic SendResponse(Byte[] data, NetworkStream stream, String response){
            data = new Byte[NumericConstants.ByteArraySize];
            response = String.Empty;
            Int32 bytes = stream.Read(data, 0, data.Length);
            response = System.Text.Encoding.ASCII.GetString(data, 0, bytes); 
            return response;            
        }
    }
}