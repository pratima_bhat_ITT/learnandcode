using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using IMQLIbrary;

namespace ClientProgram
{
    public class RequestResponseService {

        public PostMessageRequest CreateRequest() {
            PostMessageRequest request = new PostMessageRequest();
            Console.WriteLine(MessageConstants.ReadTopicName);
            request.TopicName = Console.ReadLine();
            Console.WriteLine(MessageConstants.Message);
            request.Message = Console.ReadLine();
            request.CreatedDate = DateTime.Now;
            Console.WriteLine(MessageConstants.EnterTime);
            int expiryTimeInMinutes = Convert.ToInt32(Console.ReadLine());
            request.ExpirationTime = request.CreatedDate.AddMinutes(expiryTimeInMinutes);
            return request;
        }
    
        public string GetTopicFromUser()
        {
            Console.WriteLine(MessageConstants.ReadTopicName);
            var topic = Console.ReadLine();  
            return topic ;
        }
        
        public List<TopicTable> DeserializeGetAllTopics(dynamic response)
        {
            var formattedResponse = JsonConvert.DeserializeObject<List<TopicTable>>(response);
            return formattedResponse;
        }

        public List<MessageTable> DeserializeGetAllMessages(dynamic response)
        {
            var formattedResponse = JsonConvert.DeserializeObject<List<MessageTable>>(response);
            Console.WriteLine(formattedResponse+"\n");
            return formattedResponse;
        }
    }
}