using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using IMQLIbrary;

namespace ClientProgram
{
    public class TopicService {

        public string GetAllTopics(string name, string role) 
        {
            GetTopicRequest request = new GetTopicRequest{
                clientName = name,
                RequestType = MessageConstants.GetTopic,
                Role = role
            };
            return SocketService.ConnectToServer(request);
        }

        public string SubscribeToTopic(string name, string role) 
        {
           TopicSubscriptionRequest request = new TopicSubscriptionRequest();
            request.clientName = name;
            request.RequestType = MessageConstants.SubscribeToTopic;
            request.Role = role;
            Console.WriteLine(MessageConstants.ReadSubscriberdTopicName);
            request.TopicName = Console.ReadLine();
            return SocketService.ConnectToServer(request);
        }

          public string GetAllSubscribedTopics(string name, string role) 
        {
            GetTopicRequest request = new GetTopicRequest{
                clientName = name,
                RequestType = MessageConstants.GetSubscribedTopics,
                Role = role
            };
            return SocketService.ConnectToServer(request);
        }


    }
}