using Xunit;
using ClientProgram;
using IMQLIbrary;
using System.Collections.Generic;

namespace IMQClient.Tests
{
    public class IMQServerResponseTests
    {
        private ServerResponse response = new ServerResponse();
        [Theory]
        [MemberData(nameof(ResponseData.testTopics), MemberType = typeof(ResponseData))]
        public void DisplayAllTopicsTest(List<TopicTable> topics)
        {
            response.DisplayAllTopics(topics);
            Assert.True(true);
        }

        [Theory]
        [MemberData(nameof(ResponseData.mockMessages), MemberType = typeof(ResponseData))]
        public void DisplayAllMessagesTests(List<MessageTable> messages)
        {
            response.DisplayAllMessages(messages);
            Assert.True(true);
        }
    }
}