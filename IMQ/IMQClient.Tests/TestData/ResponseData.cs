using System.Collections.Generic;
using IMQLIbrary;
using System;

namespace IMQClient.Tests {
    public class ResponseData{
        public static List<TopicTable> topics = new List<TopicTable>(){
            new TopicTable(){
                Id= 2,
                TopicName = "Learn&Grow"
            },};
            public static IEnumerable<object[]> testTopics =>
            new List<object[]>
            {
                new object [] {topics},
            
            };
            public static List<MessageTable> messages = new List<MessageTable>(){
            new MessageTable(){
                Id = 1,
                clientId = 1002,
                client = new RootTable{
                    Id = 1002,
                    ClientName = "ABC",
                    Role = "Publisher"
                },
                topicId = 1,
                topic = new TopicTable{
                    Id = 2,
                    TopicName = "Learn&Grow"
                },
                message = "Heyyy",
                createdDate = DateTime.Now,
                expirationTime = DateTime.Now,
            },
        };

        public static IEnumerable<object[]> mockMessages =>
        new List<object[]>
        {
            new object[] { messages },
        };

        public static IEnumerable<object[]> message =>
        new List<object[]>
        {
            new object[] { "Heyyy" },
        };
    }
}