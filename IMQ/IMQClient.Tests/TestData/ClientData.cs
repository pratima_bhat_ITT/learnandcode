using System.Collections.Generic;

namespace IMQClient.Tests.TestData
{
    public class ClientData
    {
        public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[] { "Akshata" },
            new object[] { "Pratima" },
            new object[] { "Abcd123" }
        };
    }
}