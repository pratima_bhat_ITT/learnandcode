using System.Reflection;
using System;
using Xunit;
using ClientProgram;

namespace IMQClient.Tests
{
    public class ClientFactoryTests
    {
        ClientFactory clientFactory = new ClientFactory(ClientRoles.Publisher);
        [Fact]
        public void GetPublisherObjectTest()
        {
            var publisher = clientFactory.GetObject(ClientRoles.Publisher);
            Assert.IsType<Publisher>(publisher);
        }

        [Fact]
        public void GetSubscriberObjectTest()
        {
            var subscriber = clientFactory.GetObject(ClientRoles.Subscriber);
            Assert.IsType<Subscriber>(subscriber);
        }

       
    }
}