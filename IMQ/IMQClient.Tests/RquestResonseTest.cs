
using System.IO;
using Xunit;
using  System;
using ClientProgram;
using IMQLIbrary ;

namespace IMQClient.Tests
{
    public class RequestResponseTest
    {
        RequestResponseService requestResponseService = new RequestResponseService();

        [Fact]
        public void CreateRequestTest()
        {
            var requestOutput = new StringWriter();
            Console.SetOut(requestOutput);
            var input = new StringReader(@"Learn&Code
            ertyu
            20");
            Console.SetIn(input);
             var request = requestResponseService.CreateRequest();
            Assert.IsType<PostMessageRequest>(request);
        }

       [Fact]
        public void GetTopicFromUserTest()
        {
            var requestOutput = new StringWriter();
            Console.SetOut(requestOutput);
            var input = new StringReader(@"Learn&Code");
            Console.SetIn(input);
            var topic = requestResponseService.GetTopicFromUser();
            Assert.IsType<string>(topic);
            string userTopic = "Learn&Code";
            Assert.Equal(userTopic, topic);
        }
    }
}
