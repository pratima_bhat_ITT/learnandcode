## Project Name

**InTimeTec Messaging Queue (IMQ)**

##Project Description

Message queues implement an asynchronous communication pattern between two or more processes/threads whereby the sending and receiving party do not need to interact with the message queue at the same time. Messages placed onto the queue are stored until the recipient retrieves them. Message queues have implicit or explicit limits on the size of data that may be transmitted in a single message and the number of messages that may remain outstanding on the queue.

## ConsoleApplication

This project was generated with **.NET CLI version 3.1** 

## Restore the Dependencies

Run **dotnet restore** to restore dependencies as well as project-specific tools that are specified in the project file.

## Build

Run **dotnet build** to build the project. The dotnet build command builds the project and its dependencies into a set of binaries. The binaries include the project's code in Intermediate Language (IL) files with a .dll extension.

## Run

Execute dotnet run to run the project.

## Running unit tests

Run **dotnet test** to execute the unit tests via XUNIT.

## Further help

To get more help on the .NET CLI, check out the https://docs.microsoft.com/en-us/dotnet/core/tools/.